clear all
close all

% Code for implementing the MH algorithm for different choices of sampler
% for problem 1 in the stochastics problem sheet.

choice=1; % 1 for MALA, 2 for MH eq (4), 3 for MH eq (6) 

% Defines some parameters depending on the choice
if choice==1
    b=0.01;
elseif choice==2
    b=0.01;
    C=[1,0;0,1];
elseif choice==3
    b=0.005;
    C=1;
else
    error('Choice must be 1, 2 or 3!')
end
    

delta=0.1;
A=1;

X0=[0,0];       % Start algorithm here

p=7;       % Number of powers of 10 of samples to take
N=10^p;     % Take 1 sample, 10 samples., 100..., 10^p sample

acc=0;      % A tally to count the number of accepted proposals

Xsamp=zeros(2,N);     % An array to store each of the samples

fm=zeros(N,1);
est=zeros(N,1);

% Burn in phase
burnin=0;
count=1;
while(count<=burnin)
    xi=[normrnd(0,1);normrnd(0,1)];     % Generates a random 2x1 vector    
    
    % An if loop to select the proposal density
    if choice==1
        Yc=Ymala(X0(1),X0(2),xi,delta,A,b);     
    elseif choice==2;
        Yc=Ymh(X0(1),X0(2),xi,delta,A,b,C);
    elseif choice==3
        Yc=YmhX(X0(1),X0(2),xi,delta,A,b,C); 
    end          
   
    u=rand(1,1);        % Generate a uniformly distributed RV
    
    alpha=min(1,(Pi(Yc(1),Yc(2),b)*q(X0,Yc,b,delta))/(Pi(X0(1),X0(2),b)*q(Yc,X0,b,delta)));
    
    if u<alpha
        X0=Yc;          % Accept new value
        acc=acc+1;
    else              % Reject new value, stay at current X
    end
    count=count+1;
end
   

%=========================================================================

n=1;
while n<=N
    xi=[normrnd(0,1);normrnd(0,1)];     % Generates a random 2x1 vector    
    
    % An if loop to select the proposal density
    if choice==1
        Yc=Ymala(X0(1),X0(2),xi,delta,A,b);     
    elseif choice==2;
        Yc=Ymh(X0(1),X0(2),xi,delta,A,b,C);
    elseif choice==3
        Yc=YmhX(X0(1),X0(2),xi,delta,A,b,C); 
    end          
   
    u=rand(1,1);        % Generate a uniformly distributed RV
    
    alpha=min(1,(Pi(Yc(1),Yc(2),b)*q(X0,Yc,b,delta))/(Pi(X0(1),X0(2),b)*q(Yc,X0,b,delta)));
    
    if u<alpha
        Xsamp(:,n)=Yc;    % Accept new value
        X0=Yc;
        acc=acc+1;
    else
        Xsamp(:,n)=X0;    % Reject new value, stay at current X
    end    
      
    fm(n,1)=f(Xsamp(1,n),Xsamp(2,n));       % Calculate f=x^2+y^2 for each sample (x,y)
        
    n=n+1;
              
end

accrate=acc/N;      % find the acceptance rate

% Now we calculate the estimator using fm
%========================================================================
   
est=zeros(N,1);      % Initialise the estimator
    
est(1,1)=fm(1,1);       % Set the first value of the estimator
l=2;
while(l<=N);     % A loop to assign the rest of the values of the estimator
    est(l,1)=est(l-1,1)+fm(l,1);
    l=l+1;
end
    
for j=1:N        % To normalise the estimator
    est(j,1)=est(j,1)/j;
end


% Confidence interval
%======================================================================

m=25;

G=Confidence(N,est,m,fm);           
    
% Plot the results
figure(1);
semilogx([1:N],est,[1:N],G(:,1),[1:N],G(:,2)); title('Estimator versus sample size');...
    xlabel('Number of samples 10^{x}');...
    saveas(figure(1),['/home/mh115/Documents/Stochastics/Code','estimator1'],'png');

pause(1)

figure(2);
plot([1:N],est,[1:N],G(:,1),[1:N],G(:,2)); title('Estimator versus sample size');...
    xlabel('Number of samples 10^{x}');...
    saveas(figure(1),['/home/mh115/Documents/Stochastics/Code','estimator2c'],'png');


% Scatter plot of distribution
%======================================================================

%figure(3)
%scatter(Xsamp(1,:),Xsamp(2,:));
    