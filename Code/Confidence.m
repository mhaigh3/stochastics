function F = Confidence(N,est,m,fm,tdelta)
% Calculates the confidence interval and stores it as two vectors
A=N/m;

fmat=zeros(A,m);  % Split fm into a matrix, easier to sum values
for column=1:m
    for row=1:A
        fmat(row,column)=fm(row+(column-1)*A,1);
    end
end

CIplus=zeros(N,1);
CIminus=zeros(N,1);
sm=zeros(N,1);


Zk=zeros(A,m);
for column=1:m
    Zk(1,column)=fmat(1,column);
    l=2;
    while(l<=A)
        Zk(l,column)=Zk(l-1,column)+fmat(l-1,column);
        l=l+1;
    end
end
for j=1:A
    Zk(j,:)=Zk(j,:)/j;
end

Z2=zeros(N,m);
for column=1:m
    Z2(1,column)=(column*Zk(1,column)-est(1,1))^2;
    l=2;
    while(l<=N/m)
        a=floor(l/m);   % n/m not necessarily an integer? Only sum up to it. 
    
        Z2(l,column)=(Zk(l-1,column)/a-est(l-1,1))^2;        
    
        l=l+1;      
    end
end
for j=1:N
    Z2(j,:)=Z2(j,:)/j;
end


for n=1:N
    sm(n,1)=(sum(Z2(n,:))/(m-1))^0.5;
         
    CIplus(n,1)=est(n,1)+tdelta*sm(n,1)/sqrt(m);
    CIminus(n,1)=est(n,1)-tdelta*sm(n,1)/sqrt(m);    
end


F=[CIplus,CIminus];

end
