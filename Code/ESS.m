function F=ESS(X,n,N)
% A function to calculate the effective sample size of a vector X
K1=150;

% define the mean at every time-step
Xbar=zeros(1,N);
Xbar(1,1)=X(1,1);       % Set the first value of the estimator
l=2;
while(l<=N);     % A loop to assign the rest of the values of the estimator
    Xbar(1,l)=Xbar(1,l-1)+X(1,l-1);
    l=l+1;
end
    
for j=1:N        % To normalise the estimator
    Xbar(1,j)=Xbar(1,j)/j;
end


% Define gamma0
gamma0=zeros(1,K1);
gamma0(1,1)=(X(1,1)-Xbar(1,1))^2;
l=2;
while(l<=K1)
    gamma0(1,l)=gamma0(1,l-1)+(X(1,l-1)-Xbar(1,l-1))^2;
    l=l+1;
end

Xik=zeros(K1,n);
for k=1:K1
    for i=1:n-K1
        Xik(k,i)=(X(1,i+k)-Xbar(1,i))*(X(1,i)-Xbar(1,i)); 
    end
end

% Find the gamma k
gammak=zeros(1,K1);
gammak(1,1)=sum(Xik(1,:));
l=2;
while(l<=K1);
    gammak(1,l)=gammak(1,l-1)+sum(Xik(l-1,:));
    l=l+1;
end

rho=zeros(1,K1);
for k=1:K1
    rho(1,k)=gammak(1,k);
end

disp(gammak);
disp(gamma0);
disp(rho);

F=1/(1+2*sum(rho(1,:)));

disp(F);
