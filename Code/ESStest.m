close all

% A code for calculating the ESS against changes in delta
% Used to tune delta.


ddelta=10;                      % Number of deltas to take
delta=linspace(0.1,1,ddelta);    % A range of delta values

N=1000;       % Number of samples to take

choice=1;       % 1 for MALA, 2 for MH eq (4), 3 for MH eq (6)

% Defines some parameters depending on the choice
if choice==1
    b=0.01;
elseif choice==2
    b=0.01;
    C=[1,0;0,1/2];
elseif choice==3
    b=0.005;
    C=[1,0;0,1];
else
    error('Choice must be 1, 2 or 3!')
end

A=1;        % Magnitude of Pi

X0=[0,0];       % Start algorithm here
Xsamp=zeros(2,N);     % An array to store each of the samples

% Burn-in phase
burnin=1000;
count=1;
while(count<=burnin)
    xi=[normrnd(0,1);normrnd(0,1)];     % Generates a random 1x2 vector    
    deltaTemp=0.749;      % A temporary value for the burn-in
    
    % An if loop to select the proposal density
    if choice==1
        Yc=Ymala(X0(1),X0(2),xi,deltaTemp,A,b);     
    elseif choice==2;
        Yc=Ymh(X0(1),X0(2),xi,deltaTemp,A,b,C);
    elseif choice==3
        Yc=YmhX(X0(1),X0(2),xi,deltaTemp,A,b,C); 
    end          
   
    u=rand(1,1);        % Generate a uniformly distributed RV
    
    alpha=min(1,(Pi(Yc(1),Yc(2),b)*qOne(X0,Yc,b,deltaTemp))/(Pi(X0(1),X0(2),b)*qOne(Yc,X0,b,deltaTemp)));
    
    if u<alpha
        X0=Yc;          % Accept new value
    else              % Reject new value, stay at current X
    end
    count=count+1;
end

acc=zeros(1,ddelta);
n=1;

j=1;

if j==1
    ESSx=zeros(1,ddelta);
    ESSy=zeros(1,ddelta);
else
end

while(n<=N)
    xi=[normrnd(0,1);normrnd(0,1)];     % Generates a random 2x1 vector    
    
    % An if loop to select the proposal density
    if choice==1
        Yc=Ymala(X0(1),X0(2),xi,delta(j),A,b);
    elseif choice==2;
        Yc=Ymh(X0(1),X0(2),xi,delta(j),A,b,C);
    elseif choice==3
        Yc=YmhX(X0(1),X0(2),xi,delta(j),A,b,C); 
    end          
   
    u=rand(1,1);        % Generate a uniformly distributed RV
    
    alpha=min(1,(Pi(Yc(1),Yc(2),b)*qOne(X0,Yc,b,delta(j)))/(Pi(X0(1),X0(2),b)*qOne(Yc,X0,b,delta(j))));
   
    if u<alpha
        Xsamp(:,n)=real(Yc);    % Accept new value
        X0=Yc;
        acc(1,j)=acc(1,j)+1;
    else
        Xsamp(:,n,j)=X0;    % Reject new value, stay at current X
    end    
     
    n=n+1;
              
end

Xvals=Xsamp(1,:);
Yvals=Xsamp(2,:);
       
ESSx(1,j)=ESS(Xvals,1000,N);
ESSy(1,j)=ESS(Yvals,1000,N);


