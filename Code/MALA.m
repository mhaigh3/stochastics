clear all
close all

% This code holds the base

choice=1; % 1 for MALA, 2 for MH eq (4), 3 for MH eq (6) 

delta=0.1;
A=1;

X0=[0,0.3];       % Start algorithm here

p=3;       % Number of powers of 10 of samples to take

acc=zeros(p+1,1);  % To start a tally for the acceptance rate

Xsamp=zeros(2,10^p,p+1);     % An array to store each of the samples


% HERE IS THE MALA CODE
%===================================================================
if choice==1
    b=0.01;
    a=0;
    while(a<=p)
        N=10^a;
        
        k=1;
        while k<=N
            xi=[normrnd(0,3),normrnd(0,3)];   % Not the right distribution yet
            
            Yc=Ymala(X0(1),X0(2),xi,delta,A,b);     % The current proposal distribution
    
            u=rand(1,1);        % Generate a uniformly distributed RV
    
            alpha=min(1,Pi(Yc(1),Yc(2),b)/Pi(X0(1),X0(2),b));
    
            u=rand;
    
            if u<alpha
                Xsamp(:,k,a+1)=Yc;    % Accept new value
                X0=Yc;
                acc(a+1,1)=acc(a+1,1)+1;
            else
                Xsamp(:,k,a+1)=X0;    % Reject new value, stay at current X, Xc
            end    
        
            k=k+1;
        end

        accrate=acc/N;
    
        a=a+1;
    
    end


    % Now we calculate the estimator

    Iest=zeros(p+1,1); % We want to Iest for each value of N
    for k=1:p+1
        m=1;
        while(m<=10^p)
            fm=f(Xsamp(1,m,k),Xsamp(2,m,k));
            Iest(k,1)=Iest(k,1)+fm;
            m=m+1;
        end
    end

    % Normalise the estimator (probably could do this in the code above)

    for a=1:p+1
        Iest(a)=Iest(a)/10^a;
    end

    % Plot the results

    figure(1);
    plot([0:p],Iest); title('Estimator versus sample size');...
        xlabel('Number of samples 10^{x}'); set(gca, 'XTick', [1:p]);...
        saveas(figure(1),['/home/mh115/Documents/Stochastics/Code','estimator1'],'png');
    
% HERE IS CODE FOR SDE (5) ON THE QUESTION SHEET
%===================================================================
else if choice==2
       b=0.01;
    a=0;
    while(a<=p)
        N=10^a;
        
        k=1;
        while k<=N
            xi=[normrnd(0,3),normrnd(0,3)];   % Not the right distribution yet
            
            Yc=Ymala(X0(1),X0(2),xi,delta,A,b,C);     % The current proposal distribution
    
            u=rand(1,1);        % Generate a uniformly distributed RV
    
            alpha=min(1,Pi(Yc(1),Yc(2),b)/Pi(X0(1),X0(2),b));
    
            u=rand;
    
            if u<alpha
                Xsamp(:,k,a+1)=Yc;    % Accept new value
                X0=Yc;
                acc(a+1,1)=acc(a+1,1)+1;
            else
                Xsamp(:,k,a+1)=X0;    % Reject new value, stay at current X, Xc
            end    
        
            k=k+1;
        end

        accrate=acc/N;
    
        a=a+1;
    
    end


    % Now we calculate the estimator

    Iest=zeros(p+1,1); % We want to Iest for each value of N
    for k=1:p+1
        m=1;
        while(m<=10^p)
            fm=f(Xsamp(1,m,k),Xsamp(2,m,k));
            Iest(k,1)=Iest(k,1)+fm;
            m=m+1;
        end
    end

    % Normalise the estimator (probably could do this in the code above)

    for a=1:p+1
        Iest(a)=Iest(a)/10^a;
    end

    % Plot the results

    figure(1);
    plot([0:p],Iest); title('Estimator versus sample size');...
        xlabel('Number of samples 10^{x}'); set(gca, 'XTick', [1:p]);...
        saveas(figure(1),['/home/mh115/Documents/Stochastics/Code','estimator1'],'png');
  
       
    else choice==3;
    
    else
    end
    
        
end

    





