function F = Pi(x,y,b)
% Target Distribution

F=exp(-(0.01*x^2+(y+b*x^2-100*b)^2));       % The target distribution

end
