function F = PiTwoT(x,J,K,Z,T)
% Target Distribution

a=0.5*J/(K*T);

A=zeros(K,K);
for l=1:K
    for j=1:K
        A(l,j)=x(l,1)*x(j,1);
    end
end

Tot=sum(A);        

F=exp(a*Tot)/Z;

end