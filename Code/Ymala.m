function F = Ymala(x,y,xi,delta,A,b)
% The MALA target distribution

d=(y+b*x^2-100*b);

grad=delta*[-0.02*x-4*b*x*d,-2*d];

F=[x,y]+grad+(2*delta)^0.5*transpose(xi);

end