function F = Ymh(x,y,xi,delta,A,b,C)
% The taget distribution from SDE (5) in the problem sheet.
M=C*transpose(C);

d=(y+b*x^2-100*b);

grad=delta*[-0.02*x-4*b*x*d;-2*d];

F=[x,y]+transpose(M*grad)+(2*delta)^0.5*transpose(C*xi);

end