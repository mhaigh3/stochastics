clear all
close all

% A code for testing the acceptance rate against changes in delta
% Used to tune delta, although a large acceptance rate does not necessarily
% mean the most useful algorithm.

runs=10;    % Number of runs to do
p=5;        % Number of powers of 10 either side of zero to take for delta
N=1000;       % Number of samples to take

choice=2;       % 1 for MALA, 2 for MH eq (4), 3 for MH eq (6)


% Defines some parameters depending on the choice
if choice==1
    b=0.01;
elseif choice==2
    b=0.01;
    C=[1,0;0,1/2];
elseif choice==3
    b=0.005;
    C=[1,0;0,1];
else
    error('Choice must be 1, 2 or 3!')
end

A=1;        % Magnitude of Pi

X0=[0,0];       % Start algorithm here
Xsamp=zeros(2,N);     % An array to store each of the samples

acc=zeros(1+2*p,runs);  % To start a tally for the acceptance rate

% Burn-in phase
burnin=1000;
count=1;
while(count<=burnin)
    xi=[normrnd(0,1);normrnd(0,1)];     % Generates a random 1x2 vector    
    delta=0.5;      % A temporary value for the burn-in
    
    % An if loop to select the proposal density
    if choice==1
        Yc=Ymala(X0(1),X0(2),xi,delta,A,b);     
    elseif choice==2;
        Yc=Ymh(X0(1),X0(2),xi,delta,A,b,C);
    elseif choice==3
        Yc=YmhX(X0(1),X0(2),xi,delta,A,b,C); 
    end          
   
    u=rand(1,1);        % Generate a uniformly distributed RV
    
    alpha=min(1,(Pi(Yc(1),Yc(2),b)*q(X0,Yc,b,delta))/(Pi(X0(1),X0(2),b)*q(Yc,X0,b,delta)));
    
    if u<alpha
        X0=Yc;          % Accept new value
    else              % Reject new value, stay at current X
    end
    count=count+1;
end

run=1;
while(run<=runs)
    a=-p;
    while(a<=p)
        delta=10^(a/2);
    
        k=1;
        while k<=N
            xi=[normrnd(0,1);normrnd(0,1)];   % Generates a random 2x1 vector            
            
            % An if loop to select the proposal density
            if choice==1
                Yc=Ymala(X0(1),X0(2),xi,delta,A,b);     
            elseif choice==2;
                Yc=Ymh(X0(1),X0(2),xi,delta,A,b,C);
            elseif choice==3
                Yc=YmhX(X0(1),X0(2),xi,delta,A,b,C); 
            end 
    
            u=rand(1,1);        % Generate a uniformly distributed RV
    
            alpha=min(1,(Pi(Yc(1),Yc(2),b)*q(X0,Yc,b,delta))/(Pi(X0(1),X0(2),b)*q(Yc,X0,b,delta)));
    
            u=rand;
    
            if u<alpha
                Xsamp(:,k)=Yc;    % Accept new value
                X0=Yc;
                acc(a+p+1,run)=acc(a+p+1,run)+1;
            else
                Xsamp(:,k)=X0;    % Reject new value, stay at current X, Xc
            end    
        
            k=k+1;
        
        end
        
        a=a+1;
    
    end
    
    

    accrate=acc/N;
    
    run=run+1;
    
end

% This is set up for 10 runs, will produce error if runs is not 10
figure(1);
plot([-p:p],accrate(:,1),[-p:p],accrate(:,2),[-p:p],accrate(:,3),...
    [-p:p],accrate(:,4),[-p:p],accrate(:,4),[-p:p],accrate(:,5),...
    [-p:p],accrate(:,6),[-p:p],accrate(:,7),[-p:p],accrate(:,8),...
    [-p:p],accrate(:,9),[-p:p],accrate(:,10)); title('\delta-dependency test for proposal (4)');
    xlabel('10^{x/2}'); set(gca, 'XTick', [-p:p]); saveas(figure(1),['/home/mh115/Documents/Stochastics/Code','deltatest1'],'png');

