clear all
close all

K1=10;     % Defines the four state space sizes
K2=50;
K3=100;
K4=150;

Z=1;    % Normalisation constant - not computationally required

J=40;      

N=10^6;     % Number of time-steps to take

xsamp1=zeros(K1,N);     % Initialises 4 empty arrays for samples to fil
xsamp2=zeros(K2,N);
xsamp3=zeros(K3,N);
xsamp4=zeros(K4,N);

acc1=0;         % Initialises 4 counters for the acceptance rate
acc2=0;
acc3=0;
acc4=0;

% Loop for K1=10;
%=========================================================================

x0=randi([0,1],K1,1);      % Generate an initial sample
for i=1:K1
    if x0(i)==0
        x0(i)=-1;
    else
    end
end

% Burn-in phase
burnin=1000;
count=1;
while(count<=burnin)
    r=randi([1 K1]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
    
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K1,Z)/PiTwo(x0,J,K1,Z));    
    
    if u<alpha        
        x0=y;            % Accept the sample y
    else                % Reject the sample
    end
    count=count+1;
end


n=1;
while(n<=N)    
    r=randi([1 K1]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
 
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K1,Z)/PiTwo(x0,J,K1,Z));    
    
    if u<alpha        
        xsamp1(:,n)=y;
        x0=y;            % Accept the sample y
        acc1=acc1+1;     % Add 1 to the acceptance rate
    else
        xsamp1(:,n)=x0;     % Reject the sample y, stay at x0
    end
    
    n=n+1;
end

% Loop for K2=50;
%=========================================================================

x0=randi([-1,1],K2,1);      % Generate an initial sample
for i=1:K2
    if x0(i)==0
        x0(i)=-1;
    else
    end
end

% Burn-in phase
burnin=1000;
count=1;
while(count<=burnin)
    r=randi([1 K2]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
    
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K2,Z)/PiTwo(x0,J,K2,Z));    
    
    if u<alpha        
        x0=y;            % Accept the sample y
    else                % Reject the sample
    end
    count=count+1;
end

n=1;
while(n<=N)    
    r=randi([1 K2]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
 
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K2,Z)/PiTwo(x0,J,K2,Z));    
    
    if u<alpha        
        xsamp2(:,n)=y;
        x0=y;            % Accept the sample y
        acc1=acc1+1;     % Add 1 to the acceptance rate
    else
        xsamp2(:,n)=x0;     % Reject the sample y, stay at x0
    end
    
    n=n+1;
end

% Loop for K3=100;
%=========================================================================

x0=randi([-1,1],K3,1);      % Generate an initial sample
for i=1:K3
    if x0(i)==0
        x0(i)=-1;
    else
    end
end

% Burn-in phase
burnin=1000;
count=1;
while(count<=burnin)
    r=randi([1 K3]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
    
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K3,Z)/PiTwo(x0,J,K3,Z));    
    
    if u<alpha        
        x0=y;            % Accept the sample y
    else                % Reject the sample
    end
    count=count+1;
end

n=1;
while(n<=N)    
    r=randi([1 K3]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
 
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K3,Z)/PiTwo(x0,J,K3,Z));    
    
    if u<alpha        
        xsamp3(:,n)=y;
        x0=y;            % Accept the sample y
        acc1=acc1+1;     % Add 1 to the acceptance rate
    else
        xsamp3(:,n)=x0;     % Reject the sample y, stay at x0
    end
    
    n=n+1;
end

% Loop for K4=150;
%=========================================================================

x0=randi([-1,1],K4,1);      % Generate an initial sample
for i=1:K4
    if x0(i)==0
        x0(i)=-1;
    else
    end
end

% Burn-in phase
burnin=1000;
count=1;
while(count<=burnin)
    r=randi([1 K4]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
    
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K4,Z)/PiTwo(x0,J,K4,Z));    
    
    if u<alpha        
        x0=y;            % Accept the sample y
    else                % Reject the sample
    end
    count=count+1;
end

n=1;
while(n<=N)    
    r=randi([1 K4]);     % Generate a random index
    y=x0;
    y(r,1)=-x0(r,1);    % Generate the new sample, changing the sign of the entry at that index
 
    u=rand(1,1);        % Generate a random number between 0 and 1  
    alpha=min(1,PiTwo(y,J,K4,Z)/PiTwo(x0,J,K4,Z));    
    
    if u<alpha        
        xsamp4(:,n)=y;
        x0=y;            % Accept the sample y
        acc1=acc1+1;     % Add 1 to the acceptance rate
    else
        xsamp4(:,n)=x0;     % Reject the sample y, stay at x0
    end
    
    n=n+1;
end


S=zeros(N,4);       % Compute the spin
for i=1:N
    S(i,1)=sum(xsamp1(:,i));
    S(i,2)=sum(xsamp2(:,i));
    S(i,3)=sum(xsamp3(:,i));
    S(i,4)=sum(xsamp4(:,i));
end

figure(1)
histogram(S(:,1)); title('Global spin for sample length K=10'); subplot(2,2,1);
histogram(S(:,2)); title('Global spin for sample length K=50'); subplot(2,2,2);
histogram(S(:,3)); title('Global spin for sample length K=100'); subplot(2,2,3);
histogram(S(:,4)); title('Global spin for sample length K=150'); subplot(2,2,4);



% ESS
ESSon=1;
if ESSon==1
    ESSav=zeros(1,4);
    ESSK1=zeros(1,K1);
    for l=1:K1
        ESSK1(1,l)=ESS(xsamp1(l,:),1000,N);
    end
    ESSK2=zeros(1,K2);
    for l=1:K2
        ESSK2(1,l)=ESS(xsamp2(l,:),1000,N);
    end
    ESSK3=zeros(1,K3);
    for l=1:K3
        ESSK3(1,l)=ESS(xsamp3(l,:),1000,N);
    end
    ESSK4=zeros(1,K4);
    for l=1:K4
        ESSK4(1,l)=ESS(xsamp4(l,:),1000,N);
    end
    
    % Now take the average to be the ESS for each K
    
    ESSav(1,1)=sum(ESSK1(1,:))/K1;
    ESSav(1,2)=sum(ESSK2(1,:))/K2;
    ESSav(1,3)=sum(ESSK3(1,:))/K3;
    ESSav(1,4)=sum(ESSK4(1,:))/K4;
    
end

figure(2)
plot([10,50,100,150],ESSav); title('Plot of ESS for K=10,50,100 and 150'); xlabel('Sample size K');...
    xticks([10,50,100,150]);


samplesize1=acc1+1;       % since we have to add on the initial sample, too

