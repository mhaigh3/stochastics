function F = q(X,Y,b,delta)
% Proposal density, q of x given y 

d=(Y(2)+b*Y(1)^2-100*b);

grad=delta*[-0.02*Y(1)-4*b*Y(1)*d,-2*d];

F=exp(-(norm(X-Y-grad)^2)/(4*delta));

end