\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{default}
\usepackage{amssymb}
\usepackage{graphicx}
\graphicspath{{/home/mh115/Documents/Stochastics/Images/}}
\usepackage{subfig}
\usepackage{amsmath}
\usepackage[margin=0.9in]{geometry}
\usepackage{empheq}
\usepackage{caption}
\usepackage{float}

\usepackage{fancyhdr}
 
\pagestyle{fancy}
\fancyhf{}
\rhead{Michael Haigh - 01081446}
\lhead{Computational Stochastic Processes - M4A44}
\rfoot{Page \thepage}



\numberwithin{equation}{section}

\begin{document}
 
\begin{center}
 \Large{Computational Stochastic Processes Assignment}
 \\
 \vspace{2mm}
 \large{Michael Haigh}
\end{center}
%===================================================================================================================================================================================


\section*{Problem 1}
%===================================================================================================================================================================================


For this question, we wish to sample from the distribution $\pi(x,y)$ given by
\begin{equation*}
\pi(x,y) \propto \exp{\left[-\left(\frac{x^{2}}{100}+\left(y+bx^{2}-100b\right)^{2}\right)\right]} \tag{1}
\end{equation*}
and calculate an estimate for the expectation of the function $f=x^{2}+y^{2}$ where the samples are $\pi$-distirbuted. The estimator $\hat{I}$ is an estimate for the quantity
\begin{equation*}
\hat{I}=\mathbb{E}_{\pi}[f]=\int_{\mathbb{R}^{2}}f(x,y)\pi(x,y)\, dx \, dy
\end{equation*}


Note that we need not know the normalising 
constant of $\pi$ as it will cancel 
with itself when calculating the accpetance probability $\alpha(x,y)$.

\subsection*{MALA Sampler}

For a first estimate, we will use the MALA sampler to generate samples. Given a current sample $X_{n}$, the MALA sampler generates a new sample $Y$ via the relation
\begin{equation*}
Y=X_{n}+\delta\nabla\log{\pi(X_{n})}+\sqrt{2\delta}\xi, \tag{3}
\end{equation*}
where $\delta$ is the step size and $\xi$ is a 2x1 uniformly distributed RV.
\\

\textbf{1}. I have written a code to implement the MALA algorithm using by proposal (3) in the question sheet. See the appendix for the code itself. Firstly, below is a density plot of 100,000 
samples of this sampler, starting from $(x_{0},y_{0})=(0,0)$.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{densityMALA.png}
\includegraphics[width=80mm]{densityMALA1.png}
\vspace{-2mm}
\caption{Density plot of 100,000 samples using the MALA sample defined by (3). The left-hand plot shows the density histogram and the left hand plot shows a scatter of the samples.}
\vspace{-2mm}
\label{densityMALA}
\end{center}
\end{figure}
Seeing the samples displayed in this way can help us to decide what to use for the matrices $M$ and $C$ in the later questions. Note that the numbers on the axes of the histogram (on the left) refer 
to the bin number of the 3-D histogram, as opposed to the value of the samples.
\\

\textbf{2}. Figure (\ref{essMALA}) shows plots of the effective sample sizes for the $x$- and $y$-variates of the chain. From the graphs, we can see that the ESS is maximised in the $x$-variate at 
$\delta=0.9$ and in the $y$-variate at $\delta=0.8$. So this information would suggest that a sample size close to these values would be a good choice. Further tests on the algorithm's 
$\delta$-dependency were also made.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{essMALAx.png}
\includegraphics[width=80mm]{essMALAy.png}
\vspace{-2mm}
\caption{Plots of the effective sample sizes for the $x$- and $y$-variates of the chain for 10,000 samples, with a discarded burn-in phase of 1000 samples.}
\vspace{-2mm}
\label{essMALA}
\end{center}
\end{figure}

Figure (\ref{deltatestmala}) shows a plots of the acceptance rate of 10 runs of the MALA algorithm, plotted against the time step $\delta$. The $x$-axis of the plot is plotted with a base $\sqrt{10}$ 
log scale. As expected, the graph shows that for small $\delta$ (i.e. $\delta\sim 0.01$), the acceptance rate is very high. In this case, a high acceptance rate does not necessarily imply 
that we have an efficient algorithm, as each sample is likely to be not travelling far from its source, meaning that we are not producing a varied set of samples. Conversely, for large 
$\delta$ (i.e. $\delta\sim 100$), the acceptance rate is close to zero, or even zero itself. This means that the algorithm will take a long time to produce a wide range of samples. We want to tune 
$\delta$ so that it lies somewhere between these extremes. 
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{deltatest1.png}
\vspace{-2mm}
\caption{Results of a $\delta$-dependency ($\delta$ plotted along the $x$-axis) test for the proposal (3). 10 runs of the same algorithm which plots the acceptance rate for increasing values of 
$\delta$ with 1000 timesteps. Note that the $x$-axis is in a log with base $\sqrt{10}$ scale, i.e. $x=1$ corresponds to a $\delta$ value of $10^{1/2}$.}
\label{deltatestmala}
\end{center}
\end{figure}
Quoting the lecture notes, an optimal acceptance rate is achieved when $\delta=O(d^{-1/3})$, where $d$ is the dimensionality of the samples. We have a 2-D sample size so, by the previous statement, 
an optimal value for $\delta$ would be $\delta=0.794$ to 3 decimal places. This value for $\delta$ is very close to the value of 0.8 that maximises the effective sample size of the $y$-variate. Now, 
note that $10^{a}=0.794$ when $a\approx -0.100$, or, for better comparison with figure (\ref{deltatestmala}), $10^{2b}=0.794$ when $b\approx -0.050$. So then, using (\ref{deltatestmala}) and crudely 
looking at the approximate acceptance rate then for this specific value of $x$, we find that the acceptance rate ranges between 0.4 and 0.6, ignoring the most outlying simulations. From here on, then, 
for the MALA sampler we will take $\delta=0.794$. For this value of $\delta$, the notes say that an acceptance rate $\approx 0.574$ should be expected. Comparisons with figure (\ref{deltatestmala}) 
show that the acceptance rate for my algorithm is generally close to this expected value, but is a little less than this on average.
\\

\textbf{3}. Figure (\ref{estMALA}) is a plot of the estimator for the MALA sampler for $1\le n \le 10^{6}$. (I took $10^{6}$ samples because my MATLAB code cannot produce $10^{7}$ samples within a 
reasonable time). Also plotted in the left-hand plot is the $95\%$ confidence interval. 
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{estMALA1.png}
\vspace{-2mm}
\caption{Plot of the estimator $\hat{I}$, with 95$\%$ confidence intervals, using the MALA sampler (3) to produce the samples. The code used a burn-in phase of 10,000 samples.}
\vspace{-2mm}
\label{estMALA}
\end{center}
\end{figure}
\\

\textbf{4}. Now we shall consider a Metropolis-Hastings algorithm similar to the prevous MALA sampler, but now adjusted to include the scaling/skewing effects of the constant-valued 
matrices $C$ and $M$ where $CC^{T}=M$. The new proposal is given by
\begin{equation*}
Y=X_{n}+\delta M\nabla\log{\pi(X_{n})}+\sqrt{2\delta}C\xi. \tag{4}
\end{equation*}
(This proposal has no equation number in the problem sheet - here I give it the tag (4).) In order to make a choice for M, we ought to consider what effect we would like the matrices to have on the 
new sample. For the proposal given by (3), the noise term $\xi$ has on (average) equal weighting in the $x$- and $y$-directions. This could be a slight problem if we were to take a current sample in 
one of the tails of the desnity plot (lower-left or lower-right areas of the density plot, where there still exists some samples), for example. In these tails, the forcing term is likely to cause a 
new sample to be selected further away from the centre of the plot, which is not what we want. A way arround this problem is to take $C$ (and therefore $M$) to be a diagonal matrix with large value 
in the upper left entry and a smaller value in the lower right entry. Since $M=CC^{T}=C^{2}$, $M$ will be of a similar form to $C$. Then, becuase $M$ is multipying the gradient term, it will act as a 
bias that gives extra weight to $x$-derivates relative to $y$-derivatives and therefore make samples towards the centre of the plot more likely to be proposed, and outlying samples are less likely to 
be proposed.

Looking at the right-hand density plot of figure (\ref{densityMALA}), we ought to take values for $M$ that are proportional to the scales of the range of $x$-values and $y$-values. In this, vain, 
let's take the matrix $M$, and consequently $C$, to be given by
\begin{equation*}
C=\left(
\begin{array}{cc}
1 & 0 \\
0 & 1/2 
\end{array}\right)
\quad \text{and} \quad
M=\left(
\begin{array}{cc}
1 & 0 \\
0 & 1/4 
\end{array}\right).
\end{equation*}
Here the upper-left entry of $M$ is four times the lower-left entry, since the $x$-extent is roughly four times greater than the $y$-extent in the figure (\ref{densityMALA}). Figure 
(\ref{densityYmh}) shows the resulting density plots for this choice of $C$ and $M$, again taken for 100,000 runs.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{densityYmh.png}
\includegraphics[width=80mm]{densityYmh1.png}
\vspace{-2mm}
\caption{Density plot of 100,000 samples using the MH sampler defined by (4). The left-hand plot shows the density histogram and the left hand plot shows a scatter of the samples.}
\vspace{-2mm}
\label{densityYmh}
\end{center}
\end{figure}


\textbf{5}. A $\delta$-dependecny test, exactly the same as for the proposal (3), was ran for the proposal (4). Figure (\ref{deltatestYmh}) plots the sample acceptance rate of 10 independent runs of 
the algorithm, each for 1000 timesteps. A first interesting thing to note is that the 10 runs seem to very strictly follow the same trend of acceptance rate decrease across the range of $\delta$, 
especially when compared with the results plotted in (\ref{deltatestmala}) where the 10 lines, while still follow the same general trend, are spread out more. Comparing with figure 
(\ref{deltatestmala}) further, the lines of figure (\ref{deltatestYmh}) are translated to the right, meaning that the same acceptance rate can be expected for a higher choice of $\delta$. Then, in 
order to kepe an acceptance rate between $0.4$ and $0.6$, I select $\delta=10^{1/4}=1.778$ to 3 decimal places. 
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{deltatest2.png}
\vspace{-2mm}
\caption{Results of a $\delta$-dependency test ($\delta$ plotted along the $x$-axis) for the proposal (4). 10 runs of the same algorithm which plots the acceptance rate for increasing values of 
$\delta$ with 100 timesteps.}
\label{deltatestYmh}
\end{center}
\end{figure}

\textbf{6}. Figure (\ref{estYmh1}) shows a plot of the estimator for the sampler given by equation (4) with the matrices given above. Also plotted are $95\%$ confidence intervals.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{estYmh1.png}
\vspace{-2mm}
\caption{Semi-log plot of the estimator $\hat{I}$, with $95\%$ confidence intervals using the MALA sampler (4) to produce $10^{6}$ samples. The code used a burn-in phase of 10,000 samples.}
\vspace{-2mm}
\label{estYmh1}
\end{center}
\end{figure}
\\

\textbf{7}. A first observation to make with regards to the difference in the samples produced by proposals (3) and (4) is evident when looking at their respective scatter plots. As is visible in 
figure (\ref{densityYmh}) the influence of the matrices in proposal (4) seems to sharpen the curved shape of the set of samples.
\\

The two estimators after $10^{6}$ samples  plotted in figures (\ref{estMALA}) and (\ref{estYmh}) show that both schemes produce a very similar estimate of just over 50, only a few hundredths over 50 
for the matrix-scaled proposal (4). They are also similar in the way that both schemes start to noticably convincingly converge to this number after $10^{4}$ samples. 
Notice that the burn-in phase is quite effective for this choice of $\delta$.
\\

\textbf{8}. Here we will return to the proposal (3), but with the parameter $b=0.05$. Figure (\ref{densityb005}) shows a new scatter plot of the samples and shows how this new parameter choice 
affects the overall shape of the selection of parameters.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{scatter8.png}
\vspace{-2mm}
\caption{Scatter plot of 100,000 samples using proposal (3) with $b=0.005$.}
\vspace{-2mm}
\label{densityb005}
\end{center}
\end{figure}

\textbf{9}. Here we will consider a proposal very similar to the previous one (4), but now where the matrices $M$ and $C$ depend on the current sample $X_{n}$. So the new proposal is
\begin{equation*}
Y=X_{n}+\delta M(X_{n})\nabla\log{\pi(X_{n})}+\delta\nabla\cdot M(X_{n})+\sqrt{2\delta}C(X_{n})\xi, \tag{6}
\end{equation*}
where we now have an extra divergence term becauses the matrix M is no longer constant. 

\textbf{10}.

\textbf{11}.


\section*{Problem 2}
%===================================================================================================================================================================================

 
\subsection*{The Ising Model}

For the Ising model, we define the target distribution by 
\begin{equation*}
\pi(x)=\frac{1}{Z_{K}}\exp{\left(\frac{J}{2K}\sum_{k,l}x_{k}x_{l}\right)}, \tag{7}
\end{equation*}
where $x=(x_{1},...,x_{K})$ is a sample in the state space $\mathbb{X}_{K}=\{-1,1\}^{K}$, $J>0$ and $Z_{K}$ is a normalising constant.
\\

\textbf{1}. For $x,y\in\mathbb{X}_{K}$, the propsal density $q(y|x)$ is given by
\begin{align*}
q(y|x)=\frac{1}{2^{K-1}K}=q(x|y).
\end{align*}
The proposal density is given by this because the probabilty of a sample $x$ and a proposed sample $y$ differing by only 1 minus sign (i.e. all of the entries of the two vectors are the same, aside 
from one entry which is the negation of its counterpart) is $1/2^{K-1}$. Then the probability of it being this entry which is randomly selected to be negated is $1/K$. Multiplying these two 
probabilities together gives $q(x|y)$. The proposal density is symmetric in $x,y$ so the acceptance probability is
\begin{equation*}
\alpha(x,y)=\text{min}\left(1,\frac{\pi(y)}{\pi(x)}\right),
\end{equation*}
with $\pi$ defined above.
\\

\textbf{2}. I implemented the scheme in MATLAB. See appendix for the code.
\\

\textbf{3}. Figure (\ref{histJ05}) shows histograms for sample lengths $K=10,50,100$ and $150$ of the global spin for the parameter $J=0.5$ for $10^{6}$ samples. Figure (\ref{ESSJ05}) shows  a plot 
of the effective sample size plotted against the sample length $K$. In order to produce these runs, a burn-in phase of 1000 samples was used after starting the simulation from a randomly produced 
array (of length $K$) of ones and minus ones.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=160mm]{histJ05.png}
\vspace{-2mm}
\caption{Histogram plots of the global spin for $K=10,50,100$ and $150$ for $10^{6}$ samples with $J=0.5$.}
\vspace{-2mm}
\label{histJ05}
\end{center}
\end{figure}
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{ESSJ05.png}
\vspace{-2mm}
\caption{Plot of the effective sample size against $K=10,50,100$ and $150$ for $10^{6}$ samples with $J=0.5$.}
\vspace{-2mm}
\label{ESSJ05}
\end{center}
\end{figure}
See item \textbf{4}. for further comments on these plots along with comparisons with the same pltos after an alteration of paramater: $J=1.25$.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=160mm]{histJ125.png}
\vspace{-2mm}
\caption{Histogram plots of the global spin for $K=10,50,100$ and $150$ for $10^{6}$ samples with $J=1.25$.}
\vspace{-2mm}
\label{histJ05}
\end{center}
\end{figure}
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=80mm]{ESSJ125.png}
\vspace{-2mm}
\caption{Plot of the effective sample size against $K=10,50,100$ and $150$ for $10^{6}$ samples with $J=1.25$.}
\vspace{-2mm}
\label{ESSJ05}
\end{center}
\end{figure}
\\

\textbf{4}. The same steps as above were repeated for a $J=1.25$. See figure (\ref{histJ125}) for the histograms of the global spins for the same selection of $K$, and see figure (\ref{ESSJ125}) for 
the 

\subsection*{Parallel Tempering}

\textbf{1}.

\textbf{2}.

\textbf{3}.

\textbf{4}.

\textbf{5}. Six chains running in parallel (on six processors) will ideally run six times faster than running all of the chains sequentially on a single processor. Neglecting communication time 
between processors, this speedup would be possible. In the real world, however, the communication time between processors means that parallel schemes never scale in the `optimal' way. A 
well implemented parallel scheme will run close to this 6-times speed-up rate, when compared to a poorly implemented one.

\newpage

\section*{Appendix}
%===================================================================================================================================================================================

Here I include all the code that I have written for the purposes of this assignment.

\subsection*{Problem 1}

\subsubsection*{The functions}


\subsection*{Problem 2}

\subsubsection*{Ising Model}

\subsubsection*{Parallel Tempering}

\end{document}
